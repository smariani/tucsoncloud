package casodistudio;

import base.AgenteWS;

public class Scrittore extends AgenteWS {
	
	@Override
	public void run() 
	{
		try {
			Print("Hi! :) ");
			
			Print("Mi loggo al servizio");
			// Mi loggo al servizio
			if(!operations.LogIn(username, password)) {
				Print(":Errore! non � stato possibile loggarsi correttamente.");
				return;
			}
			
			while(true) {
				Print("Voglio scrivere...");
				// Richiedo lock in scrittura della risorsa testo
				String result = operations.In(tuple_centre, "res(testo)");
				Print("Session(" + operations.GetSessionID() + ")");
				if(result.equals("")) {
					Print(":Errore! non � stato possibile fare in correttamente.");
					return;
				}else
					Print(result);
				
				try {
					Print("Scrivo ...");
					// Scrittura file fisico (simulata con uno sleep di 5 sec)
					sleep(5000);
				} catch (InterruptedException e) {
					System.out.println(agent_name + ": " + e.getMessage());
				}
				
				Print("Scrittura terminata");
				// Rilasco lock in scrittura
				result = operations.Out(tuple_centre, "res(testo)");
				Print("Session(" + operations.GetSessionID() + ")");
				if(result.equals("")) {
					Print(":Errore! non � stato possibile fare in correttamente.");
					return;
				}else
					Print(result);
				
				try {
					sleep(5000);
				} catch (InterruptedException e) {
					Print(e.getMessage());
				}
			}
		}catch(Exception e) {
			Print(e.getMessage());
		}
	}

	public Scrittore(String name){
		super(name);
	}
}
