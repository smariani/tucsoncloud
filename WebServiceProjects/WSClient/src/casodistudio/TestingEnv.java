package casodistudio;

import base.Operations;

public class TestingEnv extends Thread {
	static final String BASE_ADDRESS = "http://localhost:8080/TucsonOnCloudRESTful";
	static String username = "luca";
	static String password = "123456";
	public static void main(String[] args){
		Operations operations = new Operations(BASE_ADDRESS);
		
		// Richiedo un nuovo nodo (un nuovo account)
		//NewNode(operations);
		
		Print("Log al servizio...");
		// Mi loggo al servizio
		if(!operations.LogIn(username, password)) {
			Print("Errore! non � stato possibile loggarsi correttamente.");
			return;
		}
		Print("Log realizzato con successo! :)");
		
		Print("Inserisco la risorsa...");
		// Inserisco la risorsa
		String result = operations.Out("default", "res(testo)");
		if(result.equals("")) {
			Print("Errore! non � stato possibile fare out correttamente.");
			return;
		}else{
			Print(result);
		}
		Print("Risorsa inserita! :)");
		
		Scrittore scrittore = new Scrittore("scrittore-1");
		Lettore lettore1 = new Lettore("lettore-1");
		lettore1.start();
		Lettore lettore2 = new Lettore("lettore-2");
		lettore2.start();
		scrittore.start();
	}

	static private void NewNode(Operations op){
		if(!op.NewNode(username, password)) {
			Print("Errore! non � stato possibile creare l'account.");
			return;
		}
		Print("Creazione realizzata con successo! :)");

		try {
			sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	static private void Print(String msg){
		System.out.println("Environment: " + msg);
	}
}
