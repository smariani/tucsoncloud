package base;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class HttpClient {

	String address = null;
	String session_id = null;
	HttpURLConnection conn = null;
	
	public String Connect(String address){
		if(this.address == null || !this.address.equals(address)){
			this.address = address;
			this.session_id = null;
		}
		return InitConnection();
	}
	
	public String GetAddress(){
		return address;
	}
	
	public String GetSessionID(){
		return session_id;
	}
	
	public String InitConnection(){
		try {
			// Se non � presente un session_id lo devo settare
			if(session_id == null) {
				conn = (HttpURLConnection) new URL(address).openConnection();
				conn.setRequestMethod("GET");
				conn.connect();
				session_id = GetSessionID(conn);
			}
			conn = (HttpURLConnection) new URL(address).openConnection();
			// Se non � la prima connessione passo il mio ID di sessione
			if(session_id != null)
				conn.setRequestProperty("Cookie", "JSESSIONID=" + session_id);
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Accept", "text/xml");
			conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			conn.connect();
		} catch (IOException e) {
			System.out.println(e);
			return "";
		}
		return session_id;
	}
	
	public boolean SendDocument(Document doc){
		try {
			OutputStream out = conn.getOutputStream();
			Transformer transformer = TransformerFactory.newInstance().newTransformer(); // creo il trasformer per XML
	    	transformer.transform(new DOMSource(doc), new StreamResult(out)); //parserizzo in XML il DOM e lo inserisco nel buffer dei output verso il WS
	    	out.close();
		} catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
			System.out.println(e);
			return false;
		}
		return true;
	}
	
	public Document ReceiveDocument(){
		InputStream in;
		Document answer = null;
		try {
			in = conn.getInputStream();
			//Creo la richiesta
	        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder(); //creo il builder per DOM
			answer = builder.parse(in); //la parserizzo per renderla un DOM
	        in.close();
		} catch (IOException | ParserConfigurationException | SAXException e) {
			return answer;
		}
        return answer;
	}

	private String GetSessionID(HttpURLConnection connection) {
		String headerName = null;
		// Leggo tutti gli elementi presenti nel header
		for (int i=1; (headerName = conn.getHeaderFieldKey(i))!=null; i++)
		 	if (headerName.equalsIgnoreCase("Set-Cookie")) {  
				String cookie = conn.getHeaderField(i);
				cookie = cookie.substring(0, cookie.indexOf(";"));
		        String cookie_name = cookie.substring(0, cookie.indexOf("="));
		        String cookie_value = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
		        if(cookie_name.equalsIgnoreCase("JSESSIONID"))
		        	return cookie_value;
		 	}
		
		return "";
	}
}