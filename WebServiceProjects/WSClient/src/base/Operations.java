package base;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// Proxy primitive lato client, prepara e mappa le varie 
// primitive nel formato corretto per il web service
public class Operations extends Thread {
	
	HttpClient httpClient = null; 
	String session_id = "";
	String ws_address = null;
	
	public Operations(String ws_address)
	{
		this.ws_address = ws_address;
		httpClient = new HttpClient();
	}
	
	public String GetSessionID(){
		return session_id;
	}
	
	public boolean NewNode(String username, String password){
		session_id = httpClient.Connect(ws_address + "/Manager");
		
		Document doc = CreateDocument();
		if(doc == null)
			return false;
        Element cmd = doc.createElement("new-node"); //creo un nuovo elemento del DOM
        Element u = doc.createElement("username");
        u.setTextContent(username);
        Element p = doc.createElement("password");
        p.setTextContent(password);
        cmd.appendChild(u);
        cmd.appendChild(p);
        doc.appendChild(cmd); // inserisco il documento nel DOM
        // DEBUG ONLY
        //StampDocument(doc);
        if(!httpClient.SendDocument(doc))
        	return false;
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        if(res.getTagName().equals("ok"))
        	return true;
        else
        	return false;
	}
	
	public boolean LogIn(String username, String password) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return false;
        Element cmd = doc.createElement("log-in"); //creo un nuovo elemento del DOM
        doc.appendChild(cmd);
        Element u = doc.createElement("username");
        u.setTextContent(username);
        cmd.appendChild(u);
        Element p = doc.createElement("password");
        p.setTextContent(password);
        cmd.appendChild(p);
        // DEBUG ONLY 
        //StampDocument(doc);
        if(!httpClient.SendDocument(doc))
        	return false;
        
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element result = doc.getDocumentElement();
        String res = result.getChildNodes().item(0).getTextContent();
        if(res.equals("logged"))
        	return true;
        else
        	return false;
	}
	
	public String Out(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("out"); 
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	// Attendo la risposta
    	doc = httpClient.ReceiveDocument();
    	Element result = doc.getDocumentElement();
        return result.getChildNodes().item(0).getTextContent();
	}
	
	public String Rdp(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("rdp"); //creo un nuovo elemento del DOM
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element result = doc.getDocumentElement();
        return result.getChildNodes().item(0).getTextContent();
	}
	
	public String Rd(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("rd");
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        if(!res.getChildNodes().item(0).getTextContent().equals("ack"))
        	return "";
        
        String result="";
        
		while(true) {
			result = this.RdResult("default", "test(prova)");
			if(!result.equals("not-found"))
				break;
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
        return result;
	}
	
	public String RdResult(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("rd-result"); //creo un nuovo elemento del DOM
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        return res.getChildNodes().item(0).getTextContent();
	}
	
	public String Inp(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("inp"); //creo un nuovo elemento del DOM
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        return res.getChildNodes().item(0).getTextContent();
	}
	
	public String In(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("in");
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        
        if(!res.getChildNodes().item(0).getTextContent().equals("ack"))
        	return "";
        
    	String result;
		while(true) {
			result = this.InResult("default", tuple);
			if(!result.equals("not-found"))
				break;
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
        return result;
	}
	
	public String InResult(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return "";
        Element cmd = doc.createElement("in-result"); //creo un nuovo elemento del DOM
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return "";
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        return res.getChildNodes().item(0).getTextContent();
	}

	public List<String> Get(String tuple_centre_name){
		session_id = httpClient.Connect(ws_address + "/Proxy");
				
		Document doc = CreateDocument();
		if(doc == null)
			return null;
        Element cmd = doc.createElement("get"); //creo un nuovo elemento del DOM
        cmd.setAttribute("tc", tuple_centre_name);
        doc.appendChild(cmd); // inserisco il documento nel DOM
        if(!httpClient.SendDocument(doc))
        	return null;
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        List<String> result = new ArrayList<String>();
        NodeList node_list = res.getChildNodes();
        Node node;
        for(int i=0; (node = node_list.item(i)) != null; i++) {
        	result.add(node.getTextContent());
        }
        return result;
	}
	
	public List<String> Set(String tuple_centre_name, String tuple) {
		session_id = httpClient.Connect(ws_address + "/Proxy");
		
		Document doc = CreateDocument();
		if(doc == null)
			return null;
        Element cmd = doc.createElement("set"); 
        cmd.setAttribute("tc", tuple_centre_name);
        Element t = doc.createElement("tuple");
        t.setTextContent(tuple);
        cmd.appendChild(t);
        doc.appendChild(cmd); 
        if(!httpClient.SendDocument(doc))
        	return null;
    	// Attendo la risposta
    	doc = httpClient.ReceiveDocument();
    	Element res = doc.getDocumentElement();
    	List<String> result = new ArrayList<String>();
        NodeList node_list = res.getChildNodes();
        Node node;
        for(int i=0; (node = node_list.item(i)) != null; i++) {
        	result.add(node.getTextContent());
        }
        return result;
	}
	
	private Document CreateDocument(){
        DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		} //creo il builder per DOM
        return builder.newDocument(); //creo un nuovo DOM
	}

	private void StampDocument(Document doc){
		StreamResult result =  new StreamResult(System.out);
        Transformer transformer;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(new DOMSource(doc), result);
		} catch (TransformerException | TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
}
