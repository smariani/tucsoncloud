package base;


public class AgenteWS extends Thread {
	// Indirizzo Web Service
	protected String BASE_ADDRESS = "http://localhost:8080/TucsonOnCloudRESTful";
	protected String username = "luca";
	protected String password = "123456";
	protected String tuple_centre = "default";
	protected String agent_name;
	// Istanzio la classe per la comunicazione con il WS
	protected Operations operations;
		
	protected void Print(String msg){
		System.out.println(agent_name + ": " + msg);
	}
	
	public AgenteWS(String name){
		agent_name = name;
		operations = new Operations(BASE_ADDRESS);
	}
}
