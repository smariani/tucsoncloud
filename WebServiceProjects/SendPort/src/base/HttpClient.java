package base;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class HttpClient {

	String ADDRESS = "http://localhost:8080/TucsonOnCloudRESTful/Manager";
	HttpURLConnection conn = null;
	
	public HttpClient(String address){
		ADDRESS = address;
	}
	
	public boolean InitConnection(){
		try {
			conn = (HttpURLConnection) new URL(ADDRESS).openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Accept", "text/xml");
			conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			conn.connect();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean SendDocument(Document doc){
		try {
			OutputStream out = conn.getOutputStream();
			Transformer transformer = TransformerFactory.newInstance().newTransformer(); // creo il trasformer per XML
	    	transformer.transform(new DOMSource(doc), new StreamResult(out)); //parserizzo in XML il DOM e lo inserisco nel buffer dei output verso il WS
	    	out.close();
		} catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
			return false;
		}
		return true;
	}
	
	public Document ReceiveDocument(){
		InputStream in;
		Document answer = null;
		try {
			in = conn.getInputStream();
			//Creo la richiesta
	        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder(); //creo il builder per DOM
			answer = builder.parse(in); //la parserizzo per renderla un DOM
	        in.close();
		} catch (IOException | ParserConfigurationException | SAXException e) {
			return answer;
		}
        return answer;
	}
}
