package base;


import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Client {

	static final String BASE_ADDRESS = "http://localhost:8080/TucsonOnCloudRESTful/Manager";
	
	public static void main(String[] args) throws MalformedURLException, IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException, SAXException{
		if(args[0] == null)
			return;
		
		System.out.println("Inizializzo la connesione con :" + BASE_ADDRESS);
		HttpClient httpClient = new HttpClient(BASE_ADDRESS);
		if(!httpClient.InitConnection())
		{
			System.out.println("indirizzo non raggiungibile.");
			return;
		}
		
			
        System.out.println("Connessione stabilita");
        Document doc = CreateDocument();
        Element cmd = doc.createElement("take-port"); //creo un nuovo elemento del DOM
        cmd.setAttribute("port", args[0]);
        doc.appendChild(cmd); // inserisco il documento nel DOM
        if(!httpClient.SendDocument(doc))
        	return;
    	//Attendo la risposta
    	doc = httpClient.ReceiveDocument();
        Element res = doc.getDocumentElement();
        System.out.println(res.getTagName());
	}
	
	static private Document CreateDocument() throws ParserConfigurationException{
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder(); //creo il builder per DOM
        return builder.newDocument(); //creo un nuovo DOM
	}
	
}
